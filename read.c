#include "read.h"

uint32_t read_4byte(char * filename)
{
    FILE * file = fopen(filename, "rb");
    uint32_t buffer;
    size_t ret = 0;

    ret = fread(&buffer, 1, sizeof(uint32_t), file);
    if(ret != sizeof(uint32_t))
    {
        fprintf(stderr, "Failed to read valid value from %s.", filename);
        return 0;
    }

    return buffer;
}