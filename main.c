#include "read.h"
#include "add.h"

int main(int argc, char * argv[])
{
    uint32_t x, y, sum;

    if(argc != 3)
    {
        fprintf(stderr, "Usage: add-nbo FILE FILE\n");
    }

    x = ntohl(read_4byte(argv[1]));
    y = ntohl(read_4byte(argv[2]));
    sum = add(x, y);

    printf("%d(%x) + %d(%x) = %d(%x)\n", x, x, y, y, sum, sum);
}