CC	:= gcc

all: add-nbo

add-nbo: read.o add.o main.o
	$(CC) -o add-nbo read.o add.o main.o

main.o: read.h add.h main.c

read.o: read.h read.c

add.o: add.h add.c

clean:
	rm -f add-nbo
	rm -f *.o
